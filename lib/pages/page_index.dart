import 'package:book2_bombom_app/pages/page_goods_list.dart';
import 'package:book2_bombom_app/pages/page_home.dart';
import 'package:book2_bombom_app/pages/page_my_book.dart';
import 'package:book2_bombom_app/pages/page_star.dart';
import 'package:flutter/material.dart';

class PageIndex extends StatefulWidget {
  const PageIndex({super.key});

  @override
  State<PageIndex> createState() => _PageIndexState();
}

class _PageIndexState extends State<PageIndex> {
  int _indexPage = 0;

  final List<Widget> _pages = [Home(), Star(), PageGoodsList(), MyBook()];

  void _onNavTapped(int index) {
    setState(() {
      _indexPage = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('bombom'),
        centerTitle: true,
        actions: [
          IconButton(
              onPressed: () {

              },
              icon: Icon(Icons.search)),
        ],
        backgroundColor: Colors.blue,
      ),
      drawer: Drawer(
        child: ListView(
          children: [
            UserAccountsDrawerHeader(
              accountName: Text('기범스'),
              accountEmail: Text('qwe123@naver.com'),
              decoration: BoxDecoration(
                color: Colors.blue,
              ),
            ),
            ListTile(
              leading: Icon(Icons.home),
              iconColor: Colors.blue,
              focusColor: Colors.blue,
              title: Text('홈'),
              onTap: () {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (context) => PageIndex()));
              },
            ),
          ],
        ),
      ),
      body: _pages.elementAt(_indexPage),
      bottomNavigationBar: BottomNavigationBar(
        fixedColor: Colors.blue,
        unselectedItemColor: Colors.blueGrey,
        showUnselectedLabels: true,
        currentIndex: _indexPage,
        onTap: _onNavTapped,
        items: const [
          BottomNavigationBarItem(icon: Icon(Icons.home), label: '홈'),
          BottomNavigationBarItem(icon: Icon(Icons.star), label: '인기'),
          BottomNavigationBarItem(icon: Icon(Icons.book), label: '추천'),
          BottomNavigationBarItem(
              icon: Icon(Icons.account_circle_rounded), label: '내 서제'),
        ],
      ),
    );
  }
}
