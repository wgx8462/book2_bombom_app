import 'package:book2_bombom_app/components/components_book_list.dart';
import 'package:book2_bombom_app/model/books_item.dart';
import 'package:book2_bombom_app/pages/page_books_detail.dart';
import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';

class Home extends StatefulWidget {
  const Home({super.key});

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  final List<String> _list = [
    'assets/book1.jpg',
    'assets/book2.jpg',
    'assets/book3.jpg',
    'assets/book4.jpg',
  ];

  final List<BooksItem> _listbooks = [
    BooksItem(1, 'assets/book1.jpg', '철학은 날씨를 바꾼다.', 'slkf'),
    BooksItem(2, 'assets/book2.jpg', '호박눈의 산토끼', 'laksjdf'),
    BooksItem(3, 'assets/book3.jpg', '지긋지긋한 사람을 죽이지 않고 없애는법', 'alskdjf'),
    BooksItem(4, 'assets/book4.jpg', '축소되는 세계.', 'aldksjf'),
  ];

  final CarouselController carouselController = CarouselController();
  int currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
          child: Column(
            children: [
              Stack(
                children: [
                  CarouselSlider(
                    carouselController: carouselController,
                    options: CarouselOptions(
                      height: 300,
                      // 최대 크기로 지정
                      viewportFraction: 0.8,
                      // 이미지 100% 비율로 보여줌
                      autoPlay: true,
                      // 자동 슬라이드 허용
                      autoPlayInterval: const Duration(seconds: 3),
                      // 5초마다 자동 슬라이드
                      onPageChanged: ((index, reason) {
                        // 페이지 슬라이드 시 인덱스 변경
                        setState(() {
                          currentIndex = index;
                        });
                      }),
                    ),
                    items: _list.map((String item) {
                      return Image.asset(item,
                          fit: BoxFit.contain); // 이미지를 화면에 맞게 조절, 가로 세로 비율 무시
                    }).toList(),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children:
                        // 리스트에 있는 값을 map 함수를 이용하여 MapEntry 객체 컬렉션으로 생성된 Iterable을 List로 변환
                        _list.asMap().entries.map((entry) {
                      return Align(
                        child: GestureDetector(
                          onTap: () {
                            // 인디케이터에서 선택한 페이지로 이동
                            carouselController.animateToPage(entry.key);
                          },
                          // 인디케이터에 표시될 위젯
                          child: Container(
                            width: 8.0,
                            height: 8.0,
                            margin: EdgeInsets.fromLTRB(5, 310, 5, 0),
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: const Color.fromARGB(255, 133, 133, 133)
                                    .withOpacity(currentIndex == entry.key
                                        ? 0.9
                                        : 0.4)), // 현재 인덱스일 경우 진하게 표시
                          ),
                        ),
                      );
                    }).toList(),
                  ),
                ],
              ),
              Expanded(
                child: Container(
                  margin: EdgeInsets.fromLTRB(20, 20, 20, 0),
                  child: GridView.builder(
                    shrinkWrap: true, // child 위젯의 크기를 정해주지 않은 경우 true로 지정해야함
                    itemCount: _listbooks.length, //_listbooks 개수
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2, //개의 행에 보여줄 item 개수딩
                      childAspectRatio: 55 / 100, // 가로 세로 비율 1:1
                      mainAxisSpacing: 1, //수평 패딩
                      crossAxisSpacing: 1, //수직 패딩
                      //mainAxisExtent: main축의 사이즈 이걸 입력하면 childAspectRatio는 무시함
                    ),
                    itemBuilder: (BuildContext ctx, int index) {
                      return ComponentsBookList(
                        booksItem: _listbooks[index],
                        callback: () {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => PageBooksDetail(
                                  booksItem: _listbooks[index])));
                        },
                      );
                    },
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
