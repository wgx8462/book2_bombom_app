import 'package:flutter/material.dart';

class PageViewDetails extends StatefulWidget {
  const PageViewDetails({super.key});

  @override
  State<PageViewDetails> createState() => _PageViewDetailsState();
}

class _PageViewDetailsState extends State<PageViewDetails> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue,
        centerTitle: true,
        title: Text(
          'BomBom',
          style: TextStyle(
            fontWeight: FontWeight.bold,
          ),
        ),
        elevation: 0.0,
        actions: [
          IconButton(
            onPressed: () {},
            icon: Icon(Icons.search),
          ),
        ],
      ),
      drawer: Drawer(),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Image.asset(
              'assets/book1.jpg',
              width: 1000,
              height: 500,
              fit: BoxFit.contain,
            ),
            Text('철학은 날씨를 바꾼다'),
            Container(
              child: Row(
                children: [
                  Text('작가 : 김기범'),
                  Text('★★★★★ 9.7'),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
