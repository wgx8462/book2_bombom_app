import 'package:book2_bombom_app/model/books_item.dart';
import 'package:flutter/material.dart';

class PageBooksDetail extends StatefulWidget {
  const PageBooksDetail({
    super.key,
    required this.booksItem
  });

  final BooksItem booksItem;

  @override
  State<PageBooksDetail> createState() => _PageBooksDetailState();
}

class _PageBooksDetailState extends State<PageBooksDetail> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('상품 상세보기'),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Text('${widget.booksItem.id}'),
            Image.asset(widget.booksItem.imgUrl),
            Text(widget.booksItem.booksTitle),
            Text(widget.booksItem.booksText)
          ],
        ),
      ),
    );
  }
}
