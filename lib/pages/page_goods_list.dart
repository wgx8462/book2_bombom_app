import 'package:book2_bombom_app/components/component_goods_item.dart';
import 'package:book2_bombom_app/model/goods_item.dart';
import 'package:book2_bombom_app/pages/page_goods_detail.dart';
import 'package:flutter/material.dart';

class PageGoodsList extends StatefulWidget {
  const PageGoodsList({super.key});

  @override
  State<PageGoodsList> createState() => _PageGoodsListState();
}

class _PageGoodsListState extends State<PageGoodsList> {
  List<GoodsItem> _list = [
    GoodsItem(1, 'assets/book1.jpg', '재미있는 책', 12000),
    GoodsItem(2, 'assets/book2.jpg', '재미있는 책', 15000),
    GoodsItem(3, 'assets/book3.jpg', '재미있는 책', 18000),
    GoodsItem(4, 'assets/book4.jpg', '재미있는 책', 12000),
    GoodsItem(5, 'assets/book1.jpg', '재미있는 책', 15000),
    GoodsItem(6, 'assets/book2.jpg', '재미있는 책', 18000),
    GoodsItem(7, 'assets/book3.jpg', '재미있는 책', 12000),
    GoodsItem(8, 'assets/book4.jpg', '재미있는 책', 15000),
    GoodsItem(9, 'assets/book1.jpg', '재미있는 책', 18000),
    GoodsItem(10, 'assets/book2.jpg', '재미있는 책', 18000),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('추천 리스트'),
      ),
      body: ListView.builder(
        itemCount: _list.length,
        itemBuilder: (BuildContext ctx, int idx) {
          return ComponentGoodsItem(
              goodsItem: _list[idx],
              callback: () {
                Navigator.of(context).push(MaterialPageRoute(builder: (context) => PageGoodsDetail(goodsItem: _list[idx])));
              }
          );
        },
      ),
    );
  }
}
