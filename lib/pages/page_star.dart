import 'package:book2_bombom_app/model/books_item.dart';
import 'package:book2_bombom_app/pages/page_books_detail.dart';
import 'package:book2_bombom_app/pages/page_view_details.dart';
import 'package:flutter/material.dart';

import '../components/components_book_list.dart';

class Star extends StatefulWidget {
  const Star({super.key});

  @override
  State<Star> createState() => _StarState();
}

class _StarState extends State<Star> {
  final List<BooksItem> _listbooks = [
    BooksItem(1, 'assets/book1.jpg', '철학은 날씨를 바꾼다.', 'slkf'),
    BooksItem(2, 'assets/book2.jpg', '호박눈의 산토끼', 'laksjdf'),
    BooksItem(3, 'assets/book3.jpg', '지긋지긋한 사람을 죽이지 않고 없애는법', 'alskdjf'),
    BooksItem(4, 'assets/book4.jpg', '축소되는 세계.', 'aldksjf'),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        margin: EdgeInsets.fromLTRB(20, 20, 20, 0),
        child: GridView.builder(
          shrinkWrap: true,
          itemCount: _listbooks.length,
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2,
            childAspectRatio: 1 / 2,
            mainAxisSpacing: 5,
            crossAxisSpacing: 10,
          ),
          itemBuilder: (BuildContext ctx, int itx) {
            return ComponentsBookList(
              booksItem: _listbooks[itx],
              callback: () {
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) =>
                        PageBooksDetail(booksItem: _listbooks[itx])));
              },
            );
          },
        ),
      ),
    );
  }
}
