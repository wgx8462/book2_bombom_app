class BooksItem {
  num id;
  String imgUrl;
  String booksTitle;
  String booksText;

  BooksItem(this.id, this.imgUrl, this.booksTitle, this.booksText);
}