import 'package:book2_bombom_app/model/goods_item.dart';
import 'package:flutter/material.dart';

class ComponentGoodsItem extends StatelessWidget {
  const ComponentGoodsItem(
      {super.key, required this.goodsItem, required this.callback});

  final GoodsItem goodsItem;
  final VoidCallback callback;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: callback,
      child: Container(
        alignment: Alignment.center,
        child: Column(
          children: [
            SizedBox(
              width: 500,
              height: 250,
              child: Image.asset(goodsItem.imgUrl),
            ),
            Container(
              margin: EdgeInsets.fromLTRB(0, 15, 0, 15),
              child: Column(
                children: [
                  Text(goodsItem.goodsTitle),
                  Text('${goodsItem.goodsPrice}원'),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
