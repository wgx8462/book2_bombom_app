import 'package:book2_bombom_app/model/books_item.dart';
import 'package:flutter/material.dart';

class ComponentsBookList extends StatelessWidget {
  const ComponentsBookList(
      {super.key, required this.booksItem, required this.callback});

  final BooksItem booksItem;
  final VoidCallback callback;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: callback,
      child: Container(
        child: Column(
          children: [
            Container(
              child: Image.asset(
                booksItem.imgUrl,
                width: 500,
                height: 250,
              ),
            ),
            Container(
              child: Text(booksItem.booksTitle),
            ),
            Container(
              child: Text(booksItem.booksText),
            ),
          ],
        ),
      ),
    );
  }
}
